function tellFortune(numberOfChildren, partnersName, geographicLocation, jobTitle) {

    console.log(`You will be a ${jobTitle} in ${geographicLocation}, and married to ${partnersName} with ${numberOfChildren} kids`);

}

tellFortune(3, "Joanne", "Mumbai", "Secretary");
tellFortune(1, "George", "Greenland", "Archeologist");
tellFortune(2, "Jane", "Netherlands", "Engeneer");

function calculateDogAge(puppyAge, humanAge) {

    console.log(`Your doggie is ${puppyAge * 7} years in human years`);
    console.log(`You would be ${Math.round(humanAge / 7)} years in dog years`);

}

calculateDogAge(4, 35);
calculateDogAge(7, 82);
calculateDogAge(10, 12);

function calculateSupply(age, ammountPerDay) {

    console.log(`You will need ${Math.round((ammountPerDay * 365) * age)} to last you until the ripe old age of ${age}`)

}

calculateSupply(80, 2);
calculateSupply(72, 1.4);
calculateSupply(68, 3);

function calcCircumference(radius) {

    console.log(`The circumference is ${(2 * Math.PI * (radius))}`);

}

calcCircumference(3);

function calcArea(radius) {

    console.log(`The area is ${Math.PI * Math.pow(radius, 2)}`);

}

calcArea(3)

function celsiusToFahrenheit() {

    let celsiusTemperature = 32;

    console.log(`${celsiusTemperature}°C is ${(celsiusTemperature * 1.8) + 32}°F`);

}

celsiusToFahrenheit();

function fahrenheitToCelsius() {

    let fahrenheitTemperature = 78;

    console.log(`${fahrenheitTemperature}°F is ${(fahrenheitTemperature - 32) * (5 / 9)}°C`);

}

fahrenheitToCelsius();

function squareNumber(number) {

    let square = Math.pow(number, 2);

    console.log(`The result of squaring the number ${number} is ${square}`);

    return square;

}

squareNumber(3);

function halfNumber(number) {

    let halfedNumber = number / 2
    console.log(`Half of ${number} is ${halfedNumber}`)

    return halfedNumber;

}

halfNumber(5);

function percentOf(number1, number2) {

    let percent = 100 * number1 / number2
    console.log(`${number1} is ${percent}% of ${number2}`)

    return percent;

}

percentOf(2, 4);

function areaOfCircle(radius) {

    let area = (Math.PI * Math.pow(radius, 2)).toFixed(2);

    console.log(`The area for a circle with radius ${radius} is ${area}`);

    return area;

}

areaOfCircle(2);

function calculator(number) {

    let halfedNumber = halfNumber(number);
    let squaredNumber = squareNumber(halfedNumber);
    let areaCircle = areaOfCircle(squaredNumber);
    let percentArea = percentOf(areaCircle, squaredNumber);

    console.log(`The number is ${number}, half of it is ${halfedNumber}, squared it is ${squaredNumber}, area of circle is ${areaCircle}, percent of area is ${percentArea}%`);

}

calculator(4);

function drEvil(amount) {

    if (amount === 1000000) {

        console.log(`DrEvil(${amount}): ${amount}  dollars (pinky)`);
        return amount + "dollars (pinky)";

    } else {

        console.log(`DrEvil(${amount}): ${amount}  dollars`);
        return amount + "dollars";

    }

}

drEvil(1000000);

function mixUp(string1, string2) {

    let mixed = string2.slice(0, 2) + string1.slice(2) + " " + string1.slice(0, 2) + string2.slice(2);
    console.log(mixed);
    return mixed;

}

mixUp("trei", "patru");

function fixStart (string) {

    let firstChar = string.charAt(0);
    let replaced = firstChar + string.slice(1).replace(new RegExp(firstChar, 'g'), '*');
    console.log(replaced);
    return replaced;
    
}

fixStart("inlocuit");

function verbing(string){

    if (string.slice(-3) == "ing") {

        console.log(string + "ly");
        return string + "ly";

    } 
    if (string.length >= 3) {

        console.log(string + "ing");
        return string + "ing";

    } else {

        console.log(string);
        return string;

    }

}

verbing("swi");

function notBad(string){

 let stringNot = string.indexOf("not");
 let stringBad = string.indexOf("bad");
 if (stringNot == -1 || stringBad == -1 || stringBad < stringNot) {
    console.log(string);  
    return string;
 }
 let replaced = string.slice(0, stringNot) + "good" + string.slice(stringBad + 3);
 console.log(replaced);
 return replaced;

}

notBad("The dinner is bad")