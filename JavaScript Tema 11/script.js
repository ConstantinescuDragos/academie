let json = '{"name": "Georgel", "age" : 30}';

class validatorError extends Error {
    constructor(message) {
        super(message);
        this.name = "ValidationError";
    }

    validateObject(object) {

        if (!object.name) {

            throw new validatorError("Who are you?? I see no name");

        } else if (!object.age) {

            throw new validatorError("How old are you?? I see no age");

        }

        if (typeof object.name !== "string") {

            throw new validatorError("Name is not string");
        }

        if (typeof object.age !== "number") {

            throw new validatorError("Age is not int");
        }

    }
}


function validate(json) {

    try {

        let user = JSON.parse(json);
        let validare = new validatorError("");
        validare.validateObject(user);

        alert(`${user.name}, ${user.age} ti-a trimis un mesaj`)

    } catch (error) {

        alert("Eroare la JSON aoleo: " + error.message);

    }

}

validate(json);

