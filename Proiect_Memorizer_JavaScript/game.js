const deck = document.querySelectorAll('.card');
deck.forEach(card => card.addEventListener('click', flipCard));

shuffleDeck(); // randomizes the board before each game

//get elements from dom
const p1score = document.getElementById('score1');
const p2score = document.getElementById('score2');
const gameOverMsg = document.getElementById('gameOver');
const player1Frame = document.getElementById('player1Frame');
const player2Frame = document.getElementById('player2Frame');
const threeBtn = document.getElementById('3x4');
const fourBtn = document.getElementById('4x4');
const multiplayerBtn = document.getElementById('multiplayerBtn');
const singleplayerBtn = document.getElementById('singleplayerBtn');
const gameSelect = document.getElementById('gameSelect');
const board = document.getElementById('board');
const centerBoard = document.getElementById('center-board');
const submit = document.getElementById('submit');
const player1input = document.getElementById('player1input');
const player2input = document.getElementById('player2input');
const menuText = document.getElementById('menuText');
const player1 = document.getElementById('player1');
const player2 = document.getElementById('player2');
const p1moves = document.getElementById('moves1');
const p2moves = document.getElementById('moves2');
const singleMoves = document.getElementById('singleMoves');
const singleScore = document.getElementById('singleScore');

//initialize game variables
let score1 = 0;
let score2 = 0;
let moves1 = 0;
let moves2 = 0;
let isFirstCard = false;
let first, second;
let isBoardClosed = false;
let p1Turn = true;
let single = false;
let gameSize = 0;

//event listener for multiplayer button to change elements on screen
multiplayerBtn.addEventListener('click', () => {

    multiplayerBtn.classList.add('hidden');
    singleplayerBtn.classList.add('hidden');
    player1input.classList.remove('hidden');
    player2input.classList.remove('hidden');
    submit.classList.remove('hidden');
    menuText.innerHTML = "Tell me what names do you go by";
    player1Frame.style = `box-shadow: #4bb6ff 1px 1px 30px, #888ae4 1px 1px 30px`;

})

//event listener for submit button to show selection for game board
submit.addEventListener('click', () => {

    //check if player names are set
    if (player1input.value.trim() === "" || player2input.value.trim() === "") {
        menuText.innerHTML = "Enter both player names";
        menuText.style = "color: red";
    } else {
        player1.innerHTML = player1input.value;
        player2.innerHTML = player2input.value;
        threeBtn.classList.remove('hidden');
        fourBtn.classList.remove('hidden');
        submit.classList.add('hidden');
        menuText.innerHTML = "Select game option";
    }

})

//event listener for restart button to restart game
document.getElementById('restart').addEventListener('click', () => {

    restartGame();

})

//event listener to resize the board 3X4 and start game
threeBtn.addEventListener('click', () => {

    showGame();
    for (let i = 0; i < 4; i++) {
        deck[i].classList.add('hidden');
    }
    if (single) {
        player1Frame.classList.add('hidden');
        player2Frame.classList.add('hidden');
    }
    resizeBoard();
    gameSize = 3;

})

//event listener to start game with board size 4x4
fourBtn.addEventListener('click', () => {

    showGame();
    if (single) {
        player1Frame.classList.add('hidden');
        player2Frame.classList.add('hidden');
    }
    gameSize = 4;

})

//event listener for single player button to 
singleplayerBtn.addEventListener('click', () => {

    single = true;
    multiplayerBtn.classList.add('hidden');
    singleplayerBtn.classList.add('hidden');
    threeBtn.classList.remove('hidden');
    fourBtn.classList.remove('hidden');
    menuText.innerHTML = "Select game option";
    document.querySelector('.single-info').classList.remove('hidden');


})

//function to hide all menu elements and show game board and scores
function showGame() {

    threeBtn.classList.add('hidden');
    fourBtn.classList.add('hidden');
    gameSelect.classList.add('hidden');
    player1Frame.classList.remove('hidden');
    player2Frame.classList.remove('hidden');
    centerBoard.classList.remove('hidden');
    document.getElementById('gameOptions').classList.remove('hidden');

}

//function to resize the board for 3X4 dimensions
function resizeBoard() {

    board.style = `
        height: 400px;
        grid-template-rows: repeat(3, 1fr);
        `;
}


//function to shuffle the deck so that each game has a completely different board
function shuffleDeck() {
    deck.forEach(card => {
        let randomIndex = Math.floor(Math.random() * 16);
        card.style.order = randomIndex;
    });

}

//function to flip the cards after clicking on them
function flipCard() {
    if (isBoardClosed) return;
    if (this === first) return;

    this.classList.add('flip');

    if (!isFirstCard) {
        isFirstCard = true; //first click
        first = this; // 'this' = the element that has fired the event
        return
    }

    isFirstCard = false; //second click
    second = this;

    // if the second card has been chosen, check if they match
    checkMatch();
}

//function to check if the cards match
function checkMatch() {

    if (first.dataset.id == second.dataset.id) {
        //so cards cannot be clicked again
        first.removeEventListener('click', flipCard);
        second.removeEventListener('click', flipCard);

        resetBoard();
        //if multiplayer increment score and moves for each player
        if (!single) {
            if (p1Turn) {
                score1 += 1;
                moves1++;
                p1score.innerHTML = score1;
                p1moves.innerHTML = moves1;
            }
            else {
                score2 += 1;
                moves2++;
                p2score.innerHTML = score2;
                p2moves.innerHTML = moves2;
            }
            checkGameOver();
            //increment score and moves for singleplayer
        } else {
            score1 += 1;
            moves1++;
            singleScore.innerHTML = score1;
            singleMoves.innerHTML = moves1;
            checkGameOver();
        }
    }
    else {
        //if the cards are not a match then turn them over again
        isBoardClosed = true;
        setTimeout(() => {
            first.classList.remove('flip');
            second.classList.remove('flip');
            isBoardClosed = false;
            resetBoard();
        }, 1000);
        //increment moves for each player in multiplayer even if the cards don't match
        if (!single) {
            //swap players turn
            if (p1Turn) {
                p1Turn = false;
                moves1++;
                p1moves.innerHTML = moves1;
                player2Frame.style = `box-shadow: #4bb6ff 1px 1px 30px, #888ae4 1px 1px 30px`;
                player1Frame.style = 'box-shadow: none';
            }
            else if (!p1Turn) {
                p1Turn = true;
                moves2++;
                p2moves.innerHTML = moves2;
                player1Frame.style = `box-shadow: #4bb6ff 1px 1px 30px, #888ae4 1px 1px 30px`;
                player2Frame.style = 'box-shadow: none';
            }
            //increment moves in singleplayer even if the cards don't match
        } else {
            moves1++;
            singleMoves.innerHTML = moves1;
        }
    }
}


// prevents more than 2 cards being flipped over at once since it is against the rules
function resetBoard() {
    first = null;
    second = null;
    isFirstCard = false;
    isBoardClosed = false;
}

//function to restart the game 
function restartGame() {
    score1 = 0;
    score2 = 0;
    moves1 = 0;
    moves2 = 0;
    singleMoves.innerHTML = moves1;
    singleScore.innerHTML = score1;
    p1score.innerHTML = score1;
    p1moves.innerHTML = moves1;
    p2score.innerHTML = score2;
    p2moves.innerHTML = moves2;
    gameOver.classList.add('hidden');
    deck.forEach(card => card.classList.remove('flip'));
    deck.forEach(card => card.addEventListener('click', flipCard));
    setTimeout(() => {
        shuffleDeck();
    }, 500);
    resetBoard();
}

//check if the game is over by keeping track of player score, or if it is a tie
function checkGameOver() {
    if (!single && gameSize === 4) {
        gameOver.style = 'height: 500px'
        if (score1 > 4) {

            gameOverMsg.innerHTML = `CONGRATULATIONS ${player1.innerHTML}! YOU WON!`;
            showGameResult();

        }
        else if (score2 > 4) {

            gameOverMsg.innerHTML = `CONGRATULATIONS ${player2.innerHTML}! YOU WON!`;
            showGameResult();

        } else if (score1 === 4 && score2 === 4) {
            gameOverMsg.innerHTML = `TIE`;
            showGameResult();
        }
    } else if (!single && gameSize === 3) {

        if (score1 > 3) {

            gameOverMsg.innerHTML = `CONGRATULATIONS ${player1.innerHTML}! YOU WON!`;
            showGameResult();

        }
        else if (score2 > 3) {

            gameOverMsg.innerHTML = `CONGRATULATIONS ${player2.innerHTML}! YOU WON!`;
            showGameResult();

        } else if (score1 === 3 && score2 === 3) {
            gameOverMsg.innerHTML = `TIE`;
            showGameResult();
        }
        //check if game over in single player
    } else if (single && gameSize === 4) {

        if (score1 === 8) {
            gameOver.style = 'height: 500px'
            gameOverMsg.innerHTML = `CONGRATULATIONS! YOU FINISHED THE GAME!`;
            showGameResult();

        }
    } else if (single && gameSize === 3) {

        if (score1 === 6) {
            gameOverMsg.innerHTML = `CONGRATULATIONS! YOU FINISHED THE GAME!`;
            showGameResult();

        }
    }
}

function showGameResult() {

    deck.forEach(card => card.removeEventListener('click', flipCard));
    gameOver.classList.remove('hidden');

}