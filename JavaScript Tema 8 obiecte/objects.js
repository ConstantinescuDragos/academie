let recipe = {

    title: "Lista de cumparaturi",
    servings: 3,
    ingredients: ["morcovi", "rosii", "patrunjel"]

}

console.log(recipe.title);
console.log("Servings:" + recipe.servings);
//afisam incredientele
for (let i = 0; i < recipe.ingredients.length; i++) {

    console.log(recipe.ingredients[i]);

}

let books = [

    {
        title: "Ciresarii",
        author: "Constantin Chirita",
        alreadyRead: true,
    },
    {
        title: "Colt alb",
        author: "Jack London",
        alreadyRead: true,
    }

]
//afisam fiecare obiect din array
for (let i = 0; i < books.length; i++) {

    let book = books[i];
    console.log(`${book.title} by ${book.author}`);

    if (book.alreadyRead === true) {

        console.log(`You already read ${book.title} by ${book.author}`);

    } else {

        console.log(`You still need to read ${book.title} by ${book.author}`);

    }

}

let movies = {

    title: "Avengers Endgame",
    duration: 182,
    stars: ["Robert Downey Jr", "Chris Evans", "Chris Hemsworth"]

}
function printMovies(movie) {

    console.log(`${movie.title} lasts for ${movie.duration} minutes`)
    let cast = "Stars: ";
    for (let i = 0; i < movie.stars.length; i++) {
        cast += movie.stars[i];
        //adaugam o virgula dupa fiecare element din vector
        if (i != movie.stars.length - 1) {
            cast += ", "
        }

    }
    console.log(`Stars: ${cast}`);
}

printMovies(movies);

function validateCreditCard(cardNumber) {

    //verificare daca are 16 cifre
    if (cardNumber.length != 16) {
        console.log("Card invalid");
        return false;
    }

    //verificare daca toate sunt cifre
    for (let i = 0; i < cardNumber.length; i++){

        let number = cardNumber[i];
        //convertim din string in int
        number = Number.parseInt(number);

        if(!Number.isInteger(number)){

            console.log("Card invalid");
            return false;

        }

    }

}