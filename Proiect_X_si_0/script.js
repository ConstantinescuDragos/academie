const multiBtn = document.getElementById("multiplayerBtn");
const singleBtn = document.getElementById("singleplayerBtn");
const gameSelect = document.getElementById("gameSelect");
const playersMenu = document.getElementById("playersMenu");
const player1input = document.getElementById("player1input");
const player2input = document.getElementById("player2input");
const submit = document.getElementById("submit");
let player1;
let player2;
const menuText = document.getElementById("menuText");
const multiX = document.getElementById("selectX");
const multi0 = document.getElementById("select0");
const singleX = document.getElementById("selectX");
const single0 = document.getElementById("select0");
const game = document.getElementById("game");
const resultElement = document.getElementById("result");
const restartBtn = document.getElementById("restart");
const cellsElements = document.querySelectorAll(".cell");
const menuBtn = document.getElementById("menuBtn");
const body = document.getElementById("body");

let counter = 0;
//create array to hold board
let board = [
    [0, 0, 0],
    [0, 0, 0],
    [0, 0, 0]
];

//variable to pick random value from array


//game variables
let player = 1;
let gameOver = false;
let xsau0;
let botMark;

//event listener for multiplayer button
multiBtn.addEventListener("click", () => {

    //show player input and change buttons
    player1input.classList.remove("hidden");
    player2input.classList.remove("hidden");
    multiBtn.classList.add("hidden");
    singleBtn.classList.add("hidden");
    submit.classList.remove("hidden");
    menuBtn.classList.remove("hidden");
    body.style = "background-image: url(./Images/happyFace.png)";

    //add event listener for submit button
    submit.addEventListener("click", () => {

        //save player input in variables
        player1 = document.getElementById("player1input").value;
        player2 = document.getElementById("player2input").value;
        //check if player names are set
        if (player1.trim() === "" || player2.trim() === "" ) {
            menuText.innerHTML = "Enter both player names";
            menuText.style = "color: red";
            //option to select starting symbol for first player
        } else {
            submit.classList.add("hidden");
            menuText.innerHTML = "Select simbol for first player";
            menuText.style = "color: black";
            player1input.classList.add("hidden");
            player2input.classList.add("hidden");
            multiX.classList.remove("hidden");
            multi0.classList.remove("hidden");
        }

    })

    //add event listener to the 0 button
    multi0.addEventListener("click", () => {
        
        //swap players if first player chooses 0
        player2 = player1input.value;
        player1 = player2input.value;
        gameSelect.classList.add("hidden");
        game.classList.remove("hidden");
        resultElement.innerHTML = `${player1}'s turn`;

    })

    //add event listener to the 0 button
    multiX.addEventListener("click", () => {

        //keep players in order if first player chooses x
        player1 = player1input.value;
        player2 = player2input.value;
        gameSelect.classList.add("hidden");
        game.classList.remove("hidden");
        resultElement.innerHTML = `${player1}'s turn`;

    })
    //add event listener for each cell
    cellsElements.forEach((cell, index) => {

        cell.addEventListener("click", () => {

            placeMarker(index);

        })

    })

})

//add event listener to single player button
singleBtn.addEventListener("click", () => {

    //show symbol selection
    multiBtn.classList.add("hidden");
    singleBtn.classList.add("hidden");
    menuText.innerHTML = "Select starting symbol";
    multiX.classList.remove("hidden");
    multi0.classList.remove("hidden");
    menuBtn.classList.remove("hidden");
    body.style = "background-image: url(./Images/sadTrollFace.png)";

    //add event listener to the x button
    singleX.addEventListener("click", () => {

        //show board and set the symbols to each player
        gameSelect.classList.add("hidden");
        game.classList.remove("hidden");
        xsau0 = 1;
        botMark = -1;
        player1 = "Player";
        player2 = "Bot"

    })

    //add event listener to the 0 button
    single0.addEventListener("click", () => {

        //show board, set the symbols to each player and make first move for the bot
        gameSelect.classList.add("hidden");
        game.classList.remove("hidden");
        xsau0 = -1;
        botMark = 1;
        player2 = "Player";
        player1 = "Bot"
        bot();
        drawMarkers();

    })
    
    //add event listener for each cell and make 
    cellsElements.forEach((cell, index) => {
        
        //place symbol and run bot after each click
        cell.addEventListener("click", () => {

            placeMarkerSingle(index);
            bot();
            drawMarkers();
            

        })

    })

    //restart button for singleplayer game
    restartBtn.addEventListener("click", () => {
    
        resultElement.innerHTML = "";
        counter = 0;

        //if the bot is x, make first move after restart
        if(botMark === 1){
            bot();
            drawMarkers();
        }
    
    });

})

//gets random number between 0 and 2
function getRandomIndex() {
    return Math.floor(Math.random() * (3));
}

//function to get a random position, check if it is empty and place marker there
function findEmpty() {
    const randomX = getRandomIndex();
    const randomY = getRandomIndex();
    let randomPosition = board[randomX][randomY];
    if (randomPosition === 0) {
            board[randomX][randomY] = botMark;
            counter++;
        return true;

    } else {

        return false;

    }

}

//run the function and try to find epty space as long as there is any, and game is not over
function bot() {
    let pozitii = false;
    while (!pozitii && counter != 9 && !gameOver) {
        pozitii = findEmpty();
    }

    return pozitii;
}

function placeMarkerSingle(index) {

    //determine row and column
    let col = index % 3;
    let row = (index - col) / 3;
    //check if cell is empty
    if (board[row][col] === 0 && gameOver === false) {
        board[row][col] = xsau0;
        counter++;
        //update screen
        drawMarkers();
    }

}

//function to place markers
function placeMarker(index) {
    //determine row and column
    let col = index % 3;
    let row = (index - col) / 3;
    //check if cell is empty
    if (board[row][col] === 0 && gameOver === false) {
        board[row][col] = player;
        //change player
        player *= -1;
        counter++;
        //alternate to show between player turns
        if (player === -1) {
            resultElement.innerHTML = `${player2}'s turn`;
        } else {
            resultElement.innerHTML = `${player1}'s turn`;
        }
        //update screen
        drawMarkers();
    }
}
//function to draw markers
function drawMarkers() {
    //iterate over rows
    for (let i = 0; i < 3; i++) {
        //iterate over columns
        for (let j = 0; j < 3; j++) {
            //check if player 1
            if (board[i][j] === 1) {
                //update cell class adding cross
                cellsElements[(i * 3) + j].classList.add("cross");
            } else if (board[i][j] === -1) {
                //update cell class adding circle
                cellsElements[(i * 3) + j].classList.add("circle");
            }
        }
    }
            // check if any player won
    checkResult();
}

//function to check for result
function checkResult() {
    //check rows and columns
    for (let i = 0; i < 3; i++) {
        let rowSum = board[i][0] + board[i][1] + board[i][2];
        let colSum = board[0][i] + board[1][i] + board[2][i];
        if (rowSum === 3 || colSum === 3) {
            //player 1 wins
            endGame(player1);
            return;
        } else if (rowSum === -3 || colSum === -3) {
            //player 2 wins
            endGame(player2);
            return;
        }
    }
    //check diagonals
    let diag1 = board[0][0] + board[1][1] + board[2][2];
    let diag2 = board[0][2] + board[1][1] + board[2][0];
    if (diag1 === 3 || diag2 === 3) {
        //player 1 wins
        endGame(player1);
        return;
    } else if (diag1 === -3 || diag2 === -3) {
        //player 2 wins
        endGame(player2);
        return;
    }

    //check for tie
    checkForTie();

}

//function to check for tie
function checkForTie() {

    if (board[0].indexOf(0) === -1 &&
        board[1].indexOf(0) === -1 &&
        board[2].indexOf(0) === -1) {
        endGame(0);
        console.log("E tie ba")
        return;
    }

}

//function to and game
function endGame(winner) {
    //trigger game over
    gameOver = true;
 
    //check if tie
    if (winner === 0) {
        resultElement.innerHTML = "Tie";
    } else {
        resultElement.innerHTML = `${winner} wins`;
    }

}

//add event listener to restart button
restartBtn.addEventListener("click", () => {
    //reset game board
    board = [
        [0, 0, 0],
        [0, 0, 0],
        [0, 0, 0]
    ];

    //reset variables

    player = 1;
    gameOver = false;

    //reset board classes
    cellsElements.forEach(cell => {

        cell.classList.remove("cross", "circle");

    })

    //reset result text
    resultElement.innerHTML = `${player1}'s turn`;
    counter = 0;

})

