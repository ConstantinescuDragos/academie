let radius = 5;
let diameter = 2 * radius;

alert(`The circumference is ${Math.PI * diameter}`);
alert(`The area is ${Math.PI * Math.pow(radius, 2)}`);