const spaceShuttleName = "Determination";
let shuttleSpeed = 17500;
let distanceToMars = 225000000;
let distanceToMoon = 384400;
const milesPerKm = 0.621;

let milesToMars = distanceToMars * milesPerKm;
let hoursToMars = milesToMars / shuttleSpeed;
let daysToMars = hoursToMars / 24;

alert(`${spaceShuttleName} will take ${daysToMars} days to reach Mars`);

let milesToMoon = distanceToMoon * milesPerKm;
let hoursToMoon = milesToMoon / shuttleSpeed;
let daysToMoon = hoursToMoon / 24;

alert(`${spaceShuttleName} will take ${daysToMoon} days to reach the Moon`)